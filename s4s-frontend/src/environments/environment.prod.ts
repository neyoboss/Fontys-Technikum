export const environment = {
    production: true,
    //use for local development
    //baseUrl: 'http://localhost:8080/api/',

    //use for azure deployment
    baseUrl: 'https://studs4studsback.azurewebsites.net/api/',
    geoLocationServiceUrl: 'https://api.bigdatacloud.net/data/reverse-geocode-client'
};
